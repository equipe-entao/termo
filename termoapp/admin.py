from django.contrib import admin
from .models import *

class BaseModelAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_per_page = 10

    def get_user(sel, obj):
        return obj.user.username if obj.user else ''
    get_user.short_description = 'User'

class RankingAdmin(BaseModelAdmin):
    list_display = ("id", "ranking", "get_user", "score", "coins", "created_at", "updated_at",)

admin.site.register(Ranking, RankingAdmin)

class WordAdmin(BaseModelAdmin):
    list_display = ("id", "word", "date", "created_at", "updated_at",)

admin.site.register(Word, WordAdmin)

class AttemptInLine(admin.TabularInline):
    model = Attempt
    extra = 3

class UserWordAdmin(BaseModelAdmin):
    list_display = ("id", "get_user", "get_word", "type", "score", "finished", "created_at", "updated_at",)
    inlines = [AttemptInLine]

    def get_word(sel, obj):
        return obj.word.word if obj.word else ''
    get_word.short_description = 'Word'

admin.site.register(UserWord, UserWordAdmin)
