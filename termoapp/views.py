from django.shortcuts import render, redirect
from django.views import generic
from django.views.decorators.http import require_http_methods
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from unidecode import unidecode
from .models import *
from .termo import *
from .enums import *

@require_http_methods(["POST"])
def send(request):
    request_word = ""
    for i in range(1, 6):
        request_word += (request.POST[f'letter{i}'])
    request_word = unidecode(request_word).lower().strip()
    
    user = request.user
    type = request.POST['type']
    
    if not Word.objects.filter(word=request_word).exists():
        messages.error(request, "Invalid Word")
        return redirect('termoapp:' + type)

    if type == 'termo':
        user_word = UserWord().get_daily_game(user)
    else:
        user_word = UserWord().get_extra_game(user)
        
    Attempt.objects.create(user_word=user_word, attempt=request_word)

    return redirect('termoapp:' + type)
    
@require_http_methods(["POST"])
def buy(request):
    user = request.user
    ranking = Ranking.objects.get(user=user)
    ranking.buy()
        
    return redirect('termoapp:extra')
    
def logout_user(request):
    logout(request)
    return redirect('termoapp:termo')
    
class LoginView(generic.TemplateView):
    template_name = "termoapp/login.html"

    def post(self, request):
        if 'login' in request.POST:
            return self.handle_login(request)
        elif 'register' in request.POST:
            return self.handle_register(request)

    def handle_login(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('termoapp:termo') 
        
        return render(request, self.template_name, {'login_error': 'Invalid credentials'})

    def handle_register(self, request):
        username = request.POST['username']
        password1 = request.POST['password1']
        password2 = request.POST['password2']

        if password1 != password2:
            return render(request, self.template_name, {'register_error': 'Passwords do not match'})

        if User.objects.filter(username=username).exists():
            return render(request, self.template_name, {'register_error': 'Username already exists'})

        user = User.objects.create_user(username=username, password=password1)
        user.save()

        login(request, user)
        return redirect('termoapp:termo')

class UserWordView(generic.ListView):
    model = UserWord
    template_name = 'termoapp/base.html'
    context_object_name = 'attempts'
    
    def get_queryset(self):
        user = self.request.user
        user_word = UserWord().get_daily_game(user)
        word = user_word.word
        attempts = list()            
        user_attempts = list(Attempt.objects.filter(user_word=user_word).order_by('created_at').values_list('attempt'))
        for attempt in user_attempts:
            feedback = Termo(word.word).feedback(attempt[0])
            attempts.append(feedback)
            
        return attempts

    def get_context_data(self, **kwargs):
        user = self.request.user
        has_daily_word = UserWord().has_available_daily_word()
        user_word = UserWord().get_daily_game(user)
        word = user_word.word

        context = super().get_context_data(**kwargs)
        
        attempts = context['attempts'] 
        not_attempts = [True] * (6 - len(attempts))
        correct_word = None
        
        if (attempts):
            attempt = ''.join([i[0] for i in attempts[-1]])
            win = Termo(word.word).win(attempt)
            if win and not_attempts:  
                not_attempts[0] = False
                
        if len(attempts) == 6 and not win:
            correct_word = word.word

        context['attempts'] = attempts
        context['notAttempts'] = not_attempts
        context['has_daily_word'] = has_daily_word
        context['type'] = 'termo'
        context['correct_word'] = correct_word
        context['has_game'] = user_word
        return context

class UserWordExtraView(generic.ListView):
    model = UserWord
    template_name = 'termoapp/shop.html'
    context_object_name = 'attempts'
    
    def get_queryset(self):
        user = self.request.user
        user_word = UserWord().get_extra_game(user)
        attempts = list()
        
        if user_word:
            word = user_word.word
            user_attempts = list(Attempt.objects.filter(user_word=user_word).order_by('created_at').values_list('attempt'))
            for attempt in user_attempts:
                feedback = Termo(word.word).feedback(attempt[0])
                attempts.append(feedback)
        
        return attempts

    def get_context_data(self, **kwargs):
        user = self.request.user
        ranking = Ranking.objects.get(user=user)
        has_extra_words = UserWord.has_available_extra_words(self, user)

        can_buy = ranking.can_buy()

        context = super().get_context_data(**kwargs)
        
        attempts = context['attempts'] 
        not_attempts = [True] * (6 - len(attempts))
        correct_word = None

        finished = False
        user_word = UserWord().get_extra_game(user)
        if (user_word != None):
            word = user_word.word
            finished = user_word.finished
        
        if (attempts):
            attempt = ''.join([i[0] for i in attempts[-1]])
            win = Termo(word.word).win(attempt)
            if win and not_attempts:  
                not_attempts[0] = False

        if len(attempts) == 6 and not win:
            correct_word = word.word

        context['attempts'] = attempts
        context['notAttempts'] = not_attempts
        context['coins'] = ranking.coins
        context['finished'] = finished
        context['can_buy'] = can_buy
        context['has_extra_words'] = has_extra_words
        context['type'] = 'extra'
        context['correct_word'] = correct_word
        context['has_game'] = user_word != None
        return context
    
class RankingView(generic.ListView):
    model = Ranking
    template_name = 'termoapp/ranking.html'
    context_object_name = 'rankings'

    def get_queryset(self):
        user = self.request.user
        user_ranking = Ranking.objects.get(user=user)
        top10 = list(Ranking.objects.exclude(ranking__isnull=True).order_by('ranking')[:10])
        
        if (user_ranking.ranking is not None and user_ranking.ranking > 10):
            top10.append(user_ranking)
    
        return top10