from typing import Sequence, Tuple, Generator
from unidecode import unidecode
from .enums import Feedback

class InvalidAttempt(Exception):
    pass


class ValidWords():
    def __init__(self, valid_words: set) -> None:
        self._valid_words = valid_words

    def __contains__(self, item) -> bool:
        return item in self._valid_words

    def __iter__(self):
        return iter(self._valid_words)

    def __len__(self) -> int:
        return len(self._valid_words)
    

class Result():
    def __init__(self, win, feedback) -> None:
        self._win = win
        self._feedback = feedback

    @property
    def win(self) -> bool:
        return self._win

    @property
    def feedback(self) -> Sequence[Tuple[str, Feedback]]:
        return self._feedback


class Termo():
    def __init__(self, word) -> None:
        self.word: str = word
        self.valid_words: ValidWords = None
        self.__post_init__()

    def __post_init__(self) -> None:
        self.word = unidecode(self.word).lower().strip()
        self.valid_words = ValidWords({self.word})

    def _freq(self, values) -> dict:
        result = {}
        for it in values:
            result[it] = result.get(it, 0) + 1
        return result

    def _feedback(self, guess: str) -> Generator[Tuple[str, Feedback], None, None]:
        if len(guess) != len(self.word):
            raise InvalidAttempt()
        for index, c in enumerate(guess):
            if c == self.word[index]:
                yield c, Feedback.RIGHT_PLACE.value
            elif c in self.word:
                yield c, Feedback.WRONG_PLACE.value
            else:
                yield c, Feedback.WRONG.value

    def feedback(self, guess: str) -> Sequence[Tuple[str, Feedback]]:
        guess = unidecode(guess).lower().strip()
        feedback = list(self._feedback(guess))
        word_freq = self._freq(self.word)
        feedback_freq = self._freq(map(
            lambda it: it[0],
            filter(lambda it: it[1] == Feedback.RIGHT_PLACE.value, feedback)
        ))
        for k, v in word_freq.items():
            if feedback_freq.get(k, 0) >= v:
                indexes = map(lambda it: it[0], filter(
                    lambda it: it[1][0] == k and it[1][1] == Feedback.WRONG_PLACE.value,
                    enumerate(feedback),
                ))
                for index in indexes:
                    feedback[index] = (feedback[index][0], Feedback.WRONG.value)
                
        feedback_freq_2 = self._freq(map(
            lambda it: it[0],
            filter(lambda it: it[1] == Feedback.WRONG_PLACE.value, feedback)
        ))
        for k, v in word_freq.items():
            freq_k = feedback_freq_2.get(k, 0)
            if freq_k > v:
                indexes = list(map(lambda it: it[0], filter(
                    lambda it: it[1][0] == k,
                    enumerate(feedback),
                )))
                del indexes[0:v]
                for index in indexes:
                    feedback[index] = (feedback[index][0], Feedback.WRONG.value)

        return feedback
    
    def win(self, guess: str) -> bool:
        feedback = self.feedback(guess)
        cont = 0
        for tuple in feedback:
            if tuple[1] == 1:
                cont += 1

        return cont == 5
    
    def test(self, guess: str) -> Result:
        guess = unidecode(guess).lower().strip()
        if guess not in self.valid_words:
            raise InvalidAttempt()
        return Result(
            win=self.word == guess,
            feedback=self.feedback(guess),
        )
