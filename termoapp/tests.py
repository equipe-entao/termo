from django.test import TestCase, Client
from django.contrib.auth.models import User
from .models import Ranking, Word, UserWord, Attempt
from .termo import Termo
from .enums import WordType
from django.core.exceptions import ValidationError
from django.utils import timezone
from datetime import timedelta
from django.test import TestCase
from .termo import Termo, InvalidAttempt, ValidWords
from .enums import Feedback
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist

class TermoTestCase(TestCase):

    def setUp(self):
        self.termo = Termo(word='apple')

    def test_feedback(self):
        feedback = self.termo.feedback('apelt')
        expected_feedback = [
            ('a', Feedback.RIGHT_PLACE.value),
            ('p', Feedback.RIGHT_PLACE.value),
            ('e', Feedback.WRONG_PLACE.value),
            ('l', Feedback.RIGHT_PLACE.value),
            ('t', Feedback.WRONG.value),
        ]
        self.assertEqual(feedback, expected_feedback)
        
    def test_feedback_2(self):
        termo = Termo(word='turma')
        feedback = termo.feedback('aaeee')
        expected_feedback = [
            ('a', Feedback.WRONG_PLACE.value),
            ('a', Feedback.WRONG.value),
            ('e', Feedback.WRONG.value),
            ('e', Feedback.WRONG.value),
            ('e', Feedback.WRONG.value),
        ]
        self.assertEqual(feedback, expected_feedback)

    def test_win(self):
        self.assertTrue(self.termo.win('apple'))
        self.assertFalse(self.termo.win('applt'))

    def test_test(self):
        result = self.termo.test('apple')
        self.assertTrue(result.win)
        self.assertEqual(result.feedback, [
            ('a', Feedback.RIGHT_PLACE.value),
            ('p', Feedback.RIGHT_PLACE.value),
            ('p', Feedback.RIGHT_PLACE.value),
            ('l', Feedback.RIGHT_PLACE.value),
            ('e', Feedback.RIGHT_PLACE.value),
        ])
            
    def test_contains(self):
        valid_words = ValidWords({'apple', 'banana', 'orange'})
        self.assertTrue('apple' in valid_words)
        self.assertFalse('grape' in valid_words)

    def test_len(self):
        valid_words = ValidWords({'apple', 'banana', 'orange'})
        self.assertEqual(len(valid_words), 3)

    def test_iter(self):
        valid_words = ValidWords({'apple', 'banana', 'orange'})
        self.assertEqual(set(valid_words), {'apple', 'banana', 'orange'})

    def test_invalid_attempt_test(self):
        with self.assertRaises(InvalidAttempt):
            self.termo.test('aaeeee')

    def test_invalid_attempt_feedback(self):
        with self.assertRaises(InvalidAttempt):
            self.termo.feedback('aaeeee')

    def test_(self):
        feedback = self.termo.feedback('applp')
        expected_feedback = [
            ('a', Feedback.RIGHT_PLACE.value),
            ('p', Feedback.RIGHT_PLACE.value),
            ('p', Feedback.RIGHT_PLACE.value),
            ('l', Feedback.RIGHT_PLACE.value),
            ('p', Feedback.WRONG.value),
        ]
        self.assertEqual(feedback, expected_feedback)

class SignalsTestCase(TestCase):

    def test_create_ranking_and_userword_on_user_creation(self):
        user = User.objects.create(username='signaluser')
        self.assertTrue(Ranking.objects.filter(user=user).exists())
        
class ModelsTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='testuser')
        self.daily_word = Word.objects.get(date=timezone.localdate())
        self.user_daily_word = UserWord.objects.create(user=self.user, word=self.daily_word)
        self.extra_word = Word.objects.filter(date__isnull=True).first()
        self.user_extra_word = UserWord.objects.create(user=self.user, word=self.extra_word)
        
    def test_create_word(self):
        word = Word.objects.create(word='aaaaa')
        self.assertEqual(word.word, 'aaaaa')

    def test_ranking_set_ranking(self):
        ranking = Ranking.objects.get(user=self.user)
        ranking.score = 10
        ranking.save()
        ranking.set_ranking()
        ranking = Ranking.objects.get(user=self.user)
        self.assertEqual(ranking.ranking, 1)

    def test_ranking_set_score(self):
        ranking = Ranking.objects.get(user=self.user)
        user_daily_word = UserWord.objects.get(user=self.user, word=self.daily_word)
        user_daily_word.score = 5
        user_daily_word.save()
        ranking.set_score(user_daily_word)
        self.assertEqual(ranking.score, 5)
        self.assertEqual(ranking.coins, 5)

    def test_ranking_can_buy(self):
        user_extra_word = UserWord().get_extra_game(self.user)
        user_extra_word.finished = True
        user_extra_word.save()
        user_extra_word.refresh_from_db()
        ranking = Ranking.objects.get(user=self.user)
        ranking.coins = 10
        ranking.save()
        self.assertTrue(ranking.can_buy())

    def test_ranking_buy(self):
        user_extra_word = UserWord().get_extra_game(self.user)
        user_extra_word.finished = True
        user_extra_word.save()
        user_extra_word.refresh_from_db()
        ranking = Ranking.objects.get(user=self.user)
        ranking.coins = 10
        ranking.save()
        ranking.buy()
        ranking.refresh_from_db()
        self.assertTrue(user_extra_word.finished)
        self.assertEqual(ranking.coins, 4)
        self.assertFalse(ranking.can_buy())

    def test_word_get_daily_word(self):
        self.assertEqual(Word().get_daily_word(), self.daily_word)
        
    def test_self_get_games(self):
        user2 = User.objects.create(username='testuser2')
        self.assertNotEqual(UserWord().get_daily_game(user2), None)
        self.assertEqual(UserWord().get_extra_game(user2), None)

    def test_get_user_daily_word_save(self):
        self.assertEqual(self.user_daily_word.type, WordType.DAILY.value)
        self.assertFalse(self.user_daily_word.finished)
        
    def test_get_user_extra_word_save(self):
        self.assertEqual(self.user_extra_word.type, WordType.EXTRA.value)
        self.assertFalse(self.user_extra_word.finished)

    def test_attempt_save(self):
        daily_word = Word.objects.get(date=timezone.localdate() + timedelta(days=1))
        user_daily_word = UserWord.objects.create(user=self.user, word=daily_word)
        Attempt.objects.create(user_word=user_daily_word, attempt=daily_word.word)
        ranking = Ranking.objects.get(user=self.user)
        self.assertTrue(user_daily_word.finished)
        self.assertEqual(user_daily_word.score, 6)
        self.assertEqual(ranking.score, 6)

    def test_attempt_validation_error(self):
        self.user_daily_word.finished = True
        self.user_daily_word.save()
        with self.assertRaises(ValidationError):
            Attempt.objects.create(user_word=self.user_daily_word, attempt='apple')

class ViewsTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='password')
        self.daily_word = Word.objects.get(date=timezone.localdate())
        self.extra_word = Word.objects.filter(date__isnull=True).first()

    def test_login_view(self):
        response = self.client.post(reverse('termoapp:login'), {
            'username': 'testuser',
            'password': 'password',
            'login': True
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('termoapp:termo'))

    def test_register_view(self):
        response = self.client.post(reverse('termoapp:login'), {
            'username': 'newuser',
            'password1': 'newpassword',
            'password2': 'newpassword',
            'register': True
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('termoapp:termo'))
        self.assertTrue(User.objects.filter(username='newuser').exists())

    def test_send_view(self):
        self.client.login(username='testuser', password='password')
        response = self.client.post(reverse('termoapp:send'), {
            'letter1': 'l',
            'letter2': 'i',
            'letter3': 'n',
            'letter4': 'h',
            'letter5': 'a',
            'type': 'termo'
        })
        self.assertRedirects(response, reverse('termoapp:termo'))
        self.assertTrue(Attempt.objects.filter(attempt='linha').exists())
        
    def test_send_extra_view(self):
        self.client.login(username='testuser', password='password')
        UserWord.objects.create(user=self.user, word=self.extra_word)
        response = self.client.post(reverse('termoapp:send'), {
            'letter1': self.extra_word.word[0],
            'letter2': self.extra_word.word[1],
            'letter3': self.extra_word.word[2],
            'letter4': self.extra_word.word[3],
            'letter5': self.extra_word.word[4],
            'type': 'extra'
        })
        self.assertRedirects(response, reverse('termoapp:extra'))
        self.assertTrue(Attempt.objects.filter(attempt=self.extra_word.word).exists())

    def test_buy_view(self):
        self.client.login(username='testuser', password='password')
        response = self.client.post(reverse('termoapp:buy'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Ranking.objects.get(user=self.user).coins, 0)
        
    def test_buy_view_withou_existing_user_extra_word(self):
        UserWord.objects.all().delete()
        self.client.login(username='testuser', password='password')
        response = self.client.post(reverse('termoapp:buy'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Ranking.objects.get(user=self.user).coins, 0)

    def test_logout_view(self):
        self.client.login(username='testuser', password='password')
        response = self.client.get(reverse('termoapp:logout'))
        self.assertFalse('_auth_user_id' in self.client.session)

    def test_user_word_view(self):
        self.client.login(username='testuser', password='password')
        response = self.client.get(reverse('termoapp:termo'))
        self.assertTemplateUsed(response, 'termoapp/base.html')

    def test_user_word_extra_view(self):
        self.client.login(username='testuser', password='password')
        response = self.client.get(reverse('termoapp:extra'))
        self.assertTemplateUsed(response, 'termoapp/shop.html')

    def test_ranking_view(self):
        self.client.login(username='testuser', password='password')
        response = self.client.get(reverse('termoapp:ranking'))
        self.assertTemplateUsed(response, 'termoapp/ranking.html')
        self.assertIn('rankings', response.context)
            
    def test_login_view_invalid_credentials(self):
        response = self.client.post(reverse('termoapp:login'), {
            'username': 'wronguser',
            'password': 'wrongpassword',
            'login': True
        })
        self.assertEqual(response.status_code, 200)

    def test_register_view_password_mismatch(self):
        response = self.client.post(reverse('termoapp:login'), {
            'username': 'newuser',
            'password1': 'password1',
            'password2': 'password2',
            'register': True
        })
        self.assertEqual(response.status_code, 200)
        
    def test_register_view_existing_username(self):
        response = self.client.post(reverse('termoapp:login'), {
            'username': 'testuser',
            'password1': 'password',
            'password2': 'password',
            'register': True
        })
        self.assertEqual(response.status_code, 200)

    def test_user_word_extra_view_with_attempts(self):
        self.client.login(username='testuser', password='password')
        extra_word = UserWord.objects.create(user=self.user, word=self.extra_word)
        extra_word.refresh_from_db()
        Attempt.objects.create(user_word=extra_word, attempt='extra')
        response = self.client.get(reverse('termoapp:extra'))
        self.assertTemplateUsed(response, 'termoapp/shop.html')
        self.assertIn('attempts', response.context)
        self.assertIn('finished', response.context)

    def test_user_word_view_with_attempts_finished_and_not_win(self):
        self.client.login(username='testuser', password='password')
        user_word = UserWord.objects.create(user=self.user, word=self.daily_word)
        user_word.refresh_from_db()
        Attempt.objects.create(user_word=user_word, attempt='linha')
        Attempt.objects.create(user_word=user_word, attempt='linha')
        Attempt.objects.create(user_word=user_word, attempt='linha')
        Attempt.objects.create(user_word=user_word, attempt='linha')
        Attempt.objects.create(user_word=user_word, attempt='linha')
        Attempt.objects.create(user_word=user_word, attempt='linha')
        response = self.client.get(reverse('termoapp:termo'))
        self.assertIn('correct_word', response.context)
        
    def test_ranking_view_user_not_in_top10(self):
        self.client.login(username='testuser', password='password')
        for i in range(10):
            user = User.objects.create_user(username=f'user{i}', password='password')
            ranking = Ranking.objects.get(user=user)
            ranking.score = i + 1
            ranking.save()
        user_ranking = Ranking.objects.get(user=self.user)
        user_ranking.score = 0
        user_ranking.save()
        user_ranking.set_ranking()
        user_ranking.refresh_from_db()
        response = self.client.get(reverse('termoapp:ranking'))
        self.assertTemplateUsed(response, 'termoapp/ranking.html')
        self.assertIn(user_ranking, response.context['rankings'])

    def test_get_fails(self):
        User.objects.all().delete()
        Word.objects.all().delete()
        UserWord.objects.all().delete()
        word = Word().get_daily_word()
        self.assertEqual(word, None)
        user_word = UserWord().get_daily_game(self.user)
        self.assertEqual(user_word, None)

    def test_send_fails_with_not_existent_word(self):
        self.client.login(username='testuser', password='password')
        response = self.client.post(reverse('termoapp:send'), {
            'letter1': 'x',
            'letter2': 'y',
            'letter3': 'z',
            'letter4': 'w',
            'letter5': 'q',
            'type': 'termo'
        })
        self.assertRedirects(response, reverse('termoapp:termo'))