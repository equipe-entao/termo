from django.core.validators import MinLengthValidator
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from unidecode import unidecode
from .termo import *
from .enums import *

class BaseModel(models.Model):
    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    class Meta:
        abstract = True

class Ranking(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    ranking = models.IntegerField(null=True, blank=True)
    score = models.IntegerField(default=0)
    coins = models.IntegerField(default=0)

    class Meta:
        verbose_name = "Ranking"
        verbose_name_plural = "Rankings"

    def set_ranking(self):
        rankings = list(Ranking.objects.exclude(user__username="admin").order_by('-score'))
        cont = 1
        for i, ranking in enumerate(rankings):
            if (i != 0 and ranking.score != rankings[i - 1].score):
                cont += 1
            ranking.ranking = cont
            ranking.save()

    def set_score(self, user_word):
        self.score += user_word.score
        self.coins += user_word.score
        self.save()
        self.set_ranking()

    def can_buy(self):
        user_word = UserWord().get_extra_game(self.user)
        has_extra_words = UserWord().has_available_extra_words(self.user)
        if user_word:
            return user_word.finished and self.coins >= 6 and has_extra_words
        return self.coins >= 6 and has_extra_words

    def buy(self):
        if self.can_buy():
            self.coins -= 6
            extra_game = UserWord().get_extra_game(self.user)
            if extra_game:
                word = Word.objects.get(pk=extra_game.word.id+2)
            else:
                word = Word.objects.filter(date__isnull=True).first()
            user_word = UserWord.objects.create(user=self.user, word=word)
            user_word.save()
            self.save()


class Word(BaseModel):
    word = models.CharField(max_length=5, validators=[MinLengthValidator(5)])
    date = models.DateField(null=True, blank=True)

    class Meta:
        verbose_name = "Word"
        verbose_name_plural = "Words"
        constraints = [
            models.UniqueConstraint(fields=['word'], name='unique_word')
        ]
        
    def save(self, *args, **kwargs):
        is_new = self.pk is None

        self.word = unidecode(self.word).lower().strip()
        super().save(*args, **kwargs)

        if is_new:
            users = list(User.objects.all())
            for user in users:
                UserWord.objects.create(user=user, word=self)

    def get_daily_word(self):
        try:
            date = timezone.localdate()
            daily_word = Word.objects.get(
                date__day=date.day,
                date__month=date.month,
                date__year=date.year,
            )
            return daily_word
        except Word.DoesNotExist:
            return None

class UserWord(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    type = models.CharField(max_length=5, null=True, blank=True)
    score = models.IntegerField(default=0)
    finished = models.BooleanField(default=False)

    class Meta:
        verbose_name = "UserWord"
        verbose_name_plural = "UserWords"
        constraints = [
            models.UniqueConstraint(fields=['user', 'word'], name='unique_user_word'),
        ]
    
    def save(self, *args, **kwargs):
        is_new = self.pk is None
        if is_new:
            if self.word.date:
                self.type = WordType.DAILY.value
            else:
                self.type = WordType.EXTRA.value
        super().save(*args, **kwargs)

    def get_daily_game(self, user):
        try:
            return UserWord.objects.get(user=user, type=WordType.DAILY.value, word=Word().get_daily_word())
        except UserWord.DoesNotExist:
            daily_word = Word().get_daily_word()
            if daily_word:
                new_user_word = UserWord(user=user, type=WordType.DAILY.value, word=daily_word)
                new_user_word.save()
                return new_user_word
            return None
        
    def get_extra_game(self, user):
        return UserWord.objects.filter(user=user, type=WordType.EXTRA.value).order_by('id').last()
        
    def has_available_daily_word(self):
        date = timezone.localdate()
        return Word.objects.filter(
            date__day=date.day,
            date__month=date.month,
            date__year=date.year
        ).exists()

    def has_available_extra_words(self, user):
        played_words = list(UserWord.objects.filter(
            user=user, type=WordType.EXTRA.value, finished=True
        ).values_list('word', flat=True))
        extra_words = Word.objects.filter(date__isnull=True)
        non_played_words = extra_words.exclude(word__in=played_words)
        return non_played_words.exists()
            
class Attempt(BaseModel):
    user_word = models.ForeignKey(UserWord, on_delete=models.CASCADE)
    attempt = models.CharField(max_length=5, validators=[MinLengthValidator(5)], null=True, blank=True)
    
    class Meta:
        verbose_name = "Attempt"
        verbose_name_plural = "Attempts"

    def save(self, *args, **kwargs):
        if self.user_word.finished:
            raise ValidationError("UserWord finished")
        
        super().save(*args, **kwargs)
        
        word = self.user_word.word.word
        win = Termo(word).win(self.attempt)
        attempts = len(list(Attempt.objects.filter(user_word=self.user_word)))
        if attempts == 6 or win:
            self.user_word.score = 7 - attempts if win else 0
            self.user_word.finished = True
            self.user_word.save()
            ranking = Ranking.objects.get(user=self.user_word.user)
            ranking.set_score(self.user_word)

