from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from .models import *

@receiver(post_save, sender=User)
def create_by_user(sender, instance, created, **kwargs):
    if created:
        Ranking.objects.create(user=instance)
