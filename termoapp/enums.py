from enum import Enum, auto

class Feedback(Enum):
    RIGHT_PLACE = auto()
    WRONG_PLACE = auto()
    WRONG = auto()

class WordType(Enum):
    DAILY = "daily"
    EXTRA = "extra"
