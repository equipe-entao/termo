from django.db import migrations
from django.contrib.auth.models import User
from termoapp.signals import *
from pathlib import Path
from unidecode import unidecode
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

def read_words():
    words_file = Path(__file__).parent.parent.parent / 'pt-br' / 'dicio'
    with open(words_file) as input_stream:
        for line in input_stream.readlines():
            line = unidecode(line.strip().lower())
            if len(line) != 5:
                continue
            yield line

def load_words(app, schema_editor):
    Word = app.get_model('termoapp.Word')
    local_date = timezone.localdate()
    cont = 0
    for index, word in enumerate(set(read_words())):
        if index % 2 == 0:
            date = local_date + timedelta(days=cont)
            word = Word.objects.create(
                word=word,
                date=date)
            cont += 1
        else:
            word = Word.objects.create(word=word)

class Migration(migrations.Migration):

    dependencies = [
        ('termoapp', '0011_createsuperuser'),
    ]

    operations = [
        migrations.RunPython(load_words),
    ]