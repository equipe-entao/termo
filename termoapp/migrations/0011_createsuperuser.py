from django.db import migrations
from django.contrib.auth.models import User
from termoapp.signals import *
from django.conf import settings

def create_superuser(apps, schema_editor):
    User = apps.get_model(settings.AUTH_USER_MODEL)
    Ranking = apps.get_model('termoapp.Ranking')
    user = User.objects.create_superuser('admin', '', 'bccagrupo1')
    Ranking.objects.create(user=user)

class Migration(migrations.Migration):

    dependencies = [
        ('termoapp', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_superuser),
    ]
