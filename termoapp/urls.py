from .views import *
from django.urls import path
from django.contrib.auth.decorators import login_required

app_name = "termoapp"
urlpatterns = [
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', logout_user, name="logout"),
    path('', login_required(UserWordView.as_view(), login_url="/termo/login/"), name="termo"),
    path('extra/', login_required(UserWordExtraView.as_view(), login_url="/termo/login/"), name="extra"),
    path('ranking/', login_required(RankingView.as_view(), login_url="/termo/login/"), name="ranking"),
    path('send/', login_required(send, login_url="/termo/login/"), name="send"),
    path('buy/', login_required(buy, login_url="/termo/login/"), name="buy"),
]