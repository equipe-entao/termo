FROM python:3.12-alpine as builder

RUN mkdir -p /opt/app
WORKDIR /opt/app
COPY . .

ENV PIPENV_VENV_IN_PROJECT=1

RUN python -m pip install pipenv
RUN pipenv install

FROM python:3.12-alpine

COPY --from=builder /opt/app /opt/app
WORKDIR /opt/app

ENV PATH="/opt/app/.venv/bin:$PATH"

RUN chmod +x entrypoint.sh

EXPOSE 4200

ENTRYPOINT [ "/opt/app/entrypoint.sh" ]

CMD ["python", "-m", "uvicorn", "termoproject.asgi:application", "--host", "0.0.0.0", "--port", "4200"]
